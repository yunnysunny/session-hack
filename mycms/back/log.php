<?php
require_once('config.php');

class Log {
	private $level;
	private $logName = 'map.log';
	private $handle = null;
	private static $instanceLog = null;
	private $method = '';
	private $lineno = '';
	private $className = '';
	private function __construct() {
		global $defaultLog;
		$this->level = $defaultLog;
		date_default_timezone_set("PRC");
		$logPath = realpath('logs');
		$logFile = str_replace("\\", "/",$logPath)."/".$this->logName;

		$this->handle = fopen($logFile,'a');
	}

	public static function getInstance() {
		if (is_null(self::$instanceLog)) {
			self::$instanceLog = new Log();
		}
		return self::$instanceLog;
	}

	public function setMethod($method) {
		$this->method = $method;
	}

	public function setLineno($lineno) {
		$this->lineno = $lineno;
	}

	public function setClass($class) {
		$this->className = $class;
	}
	/*
	public static function getLogger($method='',$lineno='',$class='') {
		$logger = Log::getLogger();//Fatal error: Allowed memory size of 134217728 bytes exhausted (tried to allocate
														//261904 bytes) in E:\xampp\htdocs\map\log.php on line 40
		if ($method != '') {
			$logger->setMethod($method);
		}
		if ($lineno != '') {
			$logger->setLineno($lineno);
		}
		if ($class != '') {
			$logger->setClass($class);
		}
		return $logger;
	}
	*/
	public function debug($str) {
		 $time=date('Y-m-d H:i:s');
		 if ($this->level == DEBUG && !is_null($this->handle)) {
			 fwrite($this->handle,"$time [debug] $this->className $this->method $this->lineno $str\r\n");
		 } else {
                     //echo 'error';
         }
	}
	public function error($str) {
		 $time=date('Y-m-d H:i:s');
		 if ($this->level >= ERROR && !is_null($this->handle)) {
			 fwrite($this->handle,"$time [ERROR] $this->className $this->method $this->lineno $str\r\n");
		 } else {
                     //echo 'error';
         }
	}
}

#$logger = Log::getLogger(__METHOD__,__LINE__);
$logger = Log::getInstance();
#$logger->debug('测试');
