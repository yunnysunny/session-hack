<?php
session_start(); 

require_once('globaldb.php');
require_once('config.php');
require_once('log.php');
$username = trim($_POST['username']);
$errmsg = '';

if ($db->getStatus()) {
	$sql = "select password,uid from user where username='".$username."'";
	$logger->debug($sql);
	$user = $db->dbQuery($sql,1);
	if ($user) {
		$passwd = $user['password'];
		if ($_POST['passwd'] == $passwd) {
			$_SESSION['uid'] = $user['uid'];
			$result = 0;
		} else {
			$result = 1;
			$errmsg = '密码错误';
		}
		
	} else {
		$result = 2;
		$errmsg = '当前用户不存在';
	}
} else {
	$logger->error($db->getErrmsg());
	$result = 3;
	$errmsg = '数据库连接异常';
}
header('Content-type: application/json');
echo "{\"code\":$result,\"errmsg\":\"$errmsg\"}";
