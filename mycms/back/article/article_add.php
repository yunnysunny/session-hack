<?php
session_start();
$result = array();
if (!isset($_SESSION['uid']) || !$_SESSION['uid']) {
    $result['code'] = 2;
    $result['msg'] = '尚未登录';
} else {
    $uid = $_SESSION['uid'];
    require_once('../globaldb.php');
    if (!isset($_POST['title']) || !$_POST['title']) {
        $result['code'] = 4;
        $result['msg'] = '标题为空';
        goto end;
    }
    if (!isset($_POST['content']) || !$_POST['content']) {
        $result['code'] = 4;
        $result['msg'] = '内容为空';
        goto end;
    }

    if ($db->getStatus()) {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $sql = 'insert into article(title,content,uid,create_time) values("'.$title.'","'.$content.'",'.$uid.',now())';
        $rv = $db->dbExecute($sql);
        if ($rv > 0) {
            $result['code'] = 0;
        } else {
            $result['code'] = 3;
            $result['msg'] = '插入失败';
        }
    } else {
        $result['code'] = 1;
        $result['msg'] = '数据库操作失败';
    }
}
end:
echo (json_encode($result));