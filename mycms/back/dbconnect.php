<?php
/**
 *  baiducnmap
 *
 *
 *  Copyright (c) 高阳
 *  Licensed under the MIT licenses:
 *  http://www.opensource.org/licenses/mit-license.php
 *
 * @email yunnysunny@gmail.com
 * @version 0.1
 *
 */
class dbclass{

	private $errmsg = '';
	private $initOK = true;

	public function dbConnect($host,$user,$psw,$charset='utf8')//连接服务器
	{
		if(!@mysql_connect($host,$user,$psw)){
			$this->errmsg ="$user@$host connect error!";
			$this->initOK = false;
		}
		mysql_query("set names $charset");
	}
	public function dbSelect($dbname){///选择数据库
		if(mysql_select_db($dbname)) {
		  return mysql_select_db($dbname);
		} else {
			$this->errmsg = mysql_error();
			$this->initOK = false;
			return false;
		}
	}

	/*函数名：dbQuery
	 * 作用：查询数据库记录
	 * @param String $sql 查询的SQL语句
	 * @param Int $type 查询的类型,分为两类：
	 *    1.$type=0(默认值)：返回多条记录
	 *    2.$type=1：返回一条记录
	 * @return
	 * 1.无记录：null
	 * 2.有记录，返回记录数组*
    */

	public function  dbQuery($sql,$type=0){
		if (!$this->initOK) {
			return null;
		}
		$query=mysql_query($sql);
		if(!$query){//数据库中无此记录;
			$this->errmsg = mysql_error();
			return null;
		}
		else{//查询到记录
			if($type==1){//仅让其返回一条记录的情况
				//return 1;
				$a=mysql_fetch_array($query, MYSQL_ASSOC );
				return $a;
			}//以下是让其返回多条记录的情况
		    $v = array();
		    $i=0;
		    while ($a=mysql_fetch_array($query, MYSQL_ASSOC)){
			   $v[$i]=$a;
			   $i++;
		    }
		    return $v;
		}

	}
	public function dbExecute($insertSql){
		if (!$this->initOK) {
			return -1;
		}
		@$query=mysql_query($insertSql);
        if($query){
           return mysql_affected_rows();
        }
        else{
			$this->errmsg = mysql_error();
            return -1;
        }

	}

	public function getMax($tableName,$fieldName) {
		if (!$this->initOK) {
			return -1;
		}
		$sql="select max(".$fieldName.") from ".$tableName;
		@$query=mysql_query($sql);
        if($query) {//sql语句查询成功
            $returnArray=mysql_fetch_array($query);
            return $returnArray[0]?($returnArray[0]+1):1;//返回字段的最大值，为空则返回1
        } else {//sql语句查询不成功
			$this->errmsg = mysql_error();
            return -1;
        }
	}

	public function dbClose(){
		mysql_close();
	}

	public function getErrmsg() {
		return $this->errmsg;
	}

	public function getStatus() {
		return $this->initOK;
	}
}
?>